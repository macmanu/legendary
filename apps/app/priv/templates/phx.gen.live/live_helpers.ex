defmodule <%= inspect context.web_module %>.LiveHelpers do
    @moduledoc """
  Commonly functions for LiveViews.
  """

  import Phoenix.LiveView
  import Phoenix.LiveView.Helpers

  import Legendary.CoreWeb.Helpers

  alias Phoenix.LiveView.JS

  def assign_defaults(socket, session) do
    assign_new(socket, :current_user, fn -> get_user(socket, session) end)
  end

  def require_auth(socket) do
    case socket.assigns do
      %{current_user: user} when not is_nil(user) ->
        socket
      _ ->
        redirect(socket, to: "/")
    end
  end

  defp get_user(socket, session, config \\ [otp_app: :core])

  defp get_user(socket, %{"core_auth" => signed_token}, config) do
    {otp_app, _config} = Keyword.pop(config, :otp_app, :core)
    {store, store_config} = Pow.Plug.Base.store(Application.get_env(otp_app, :pow))

    conn = struct!(Plug.Conn, secret_key_base: socket.endpoint.config(:secret_key_base))
    salt = Atom.to_string(Pow.Plug.Session)

    with {:ok, token} <- Pow.Plug.verify_token(conn, salt, signed_token, config),
        {user, _metadata} <- store.get(store_config, token) do
      user
    else
      _any -> nil
    end
  end

  defp get_user(_, _, _), do: nil

  @doc """
  Renders a live component inside a modal.

  The rendered modal receives a `:return_to` option to properly update
  the URL when the modal is closed.

  ## Examples

      <.modal return_to={Routes.<%= schema.singular %>_index_path(@socket, :index)}>
        <.live_component
          module={<%= inspect context.web_module %>.<%= inspect Module.concat(schema.web_namespace, schema.alias) %>Live.FormComponent}
          id={@<%= schema.singular %>.id || :new}
          title={@page_title}
          action={@live_action}
          return_to={Routes.<%= schema.singular %>_index_path(@socket, :index)}
          <%= schema.singular %>: @<%= schema.singular %>
        />
      </.modal>
  """
  def modal(assigns) do
    assigns = assign_new(assigns, :return_to, fn -> nil end)

    ~H"""
    <div id="modal" class="fade-in absolute inset-0 flex items-center justify-center bg-gray-700 bg-opacity-50" phx-remove={hide_modal()}>
      <div
        id="modal-content"
        class="fade-in-scale p-6 mx-auto max-w-2xl w-3/4 bg-gray-100 bg-opacity-100 shadow-lg rounded-lg"
        phx-click-away={JS.dispatch("click", to: "#close")}
        phx-window-keydown={JS.dispatch("click", to: "#close")}
        phx-key="escape"
      >
        <div class="p-6 mx-auto max-w-2xl">
          <div class="flex pb-6">
            <div class="w-/12 flex-1 text-4xl">
              <h1><%%= @title %></h1>
            </div>
            <div class="w-/12 text-4xl text-right">
              <%%= if @return_to do %>
                <%%= styled_button_live_patch "Back",
                  to: @return_to,
                  id: "close",
                  class: "phx-modal-close",
                  phx_click: hide_modal()
                %>
              <%% else %>
                <%%= styled_button_link "Back",
                  href: "#",
                  id: "close",
                  class: "phx-modal-close",
                  phx_click: hide_modal()
                %>
              <%% end %>
            </div>
          </div>
          <%%= render_slot(@inner_block) %>
        </div>
      </div>
    </div>
    """
  end

  defp hide_modal(js \\ %JS{}) do
    js
    |> JS.hide(to: "#modal", transition: "fade-out")
    |> JS.hide(to: "#modal-content", transition: "fade-out-scale")
  end
end
