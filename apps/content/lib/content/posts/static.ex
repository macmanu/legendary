defmodule Legendary.Content.Posts.Static do
  @moduledoc """
  A View which compiles heex templates from the static_pages directory and makes them available to
  the content engine.
  """

  @static_page_directory_path Path.expand("../../../../app/lib/app_web/templates/static_pages", __DIR__)
  @template_paths Path.wildcard(Path.join(@static_page_directory_path, "**/*.html.heex"))

  alias Phoenix.LiveView.HTMLEngine
  import Legendary.CoreWeb.Helpers

  for template_path <- @template_paths do
    slug = template_path |> Path.basename(".heex") |> Path.basename(".html")
    quoted = HTMLEngine.compile(template_path, slug)

    def compiled_static(unquote(slug)) do
      assigns = %{}
      unquote(quoted)
      |> Phoenix.HTML.html_escape()
    end
  end

  def compiled_static(_), do: nil

  def render(template_name, _conn) do
    compiled_static(Path.basename(template_name, ".html"))
  end
end
