defmodule Legendary.Content.SitemapView do
  use Legendary.Content, :view

   # sobelow_skip ["XSS.Raw"]
   def raw_content(text) do
    Phoenix.HTML.raw(text)
  end
end
